# std.pxd
# Copyright (c) 2004-2005 University of Maryland.
#               2006-2014 University of Southern California.
# See LICENSE.md for more information.

cdef extern from *:
    ctypedef char* const_char_ptr "const char*" # Cython FAQ

cdef extern from "std.hpp":
    pass

cdef extern from "string":
    ctypedef struct c_string "std::string":
        void assign(char *)
        void swap(c_string)
        char *c_str ()
