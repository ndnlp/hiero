import sys, re, math
import model, svector, log
import sym

# lm.pyx
# David Chiang <chiang@isi.edu>

# Copyright (c) 2004-2006 University of Maryland. All rights
# reserved. Do not redistribute without permission from the
# author. Not for commercial use.

cdef extern from "stdlib.h":
    void *malloc(int size)
    void *calloc(int nelem, int size)
    void *realloc(void *buf, int size)
    void free(void *ptr)
    
cdef extern from "ctype.h":
    int tolower(int c)

cimport _kenlm

import sym
cimport sym

cdef int START, STOP, UNKNOWN, HOLE
START = sym.fromstring("<s>")
STOP = sym.fromstring("</s>")
UNKNOWN = sym.fromstring("<unk>")
HOLE = sym.fromstring("<elided>")

import rule
cimport rule

"""strings with holes
a hole stands for a run of symbols with all their n-grams computed
material with a hole to its left is assumed to have its n-grams to the left already computed, likewise for the right side"""

# temporary area used by lookup_words
cdef int bufsize
cdef int *buf
cdef _kenlm.WordIndex *mbuf

bufsize = 5
buf = <int *>malloc(bufsize*sizeof(int))
mbuf = <_kenlm.WordIndex *>malloc(bufsize*sizeof(int))

cdef class Model:
    cdef _kenlm.Model* model
    cdef _kenlm.const_Vocabulary* vocab

    def __init__(self, path):
        try:
            self.model = _kenlm.LoadVirtual(path)
        except RuntimeError as exception:
            exception_message = str(exception).replace('\n', ' ')
            raise IOError('Cannot read model \'{}\' ({})'.format(path, exception_message))
        self.vocab = &self.model.BaseVocabulary()

    property order:
        def __get__(self):
            return self.model.Order()

    def __dealloc__(self):
        del self.model

cdef lookup_words1(Model ngram, rule.Phrase ewords, antstates, char mapdigits, bint lowercase, float ceiling):
    cdef int length, edge, i, j
    cdef float sum, p
    cdef int word
    global buf, mbuf, bufsize
    cdef int order = ngram.model.Order()
    cdef _kenlm.State dummy

    length = ewords.n-ewords.n_vars
    for words in antstates:
        i = len(words)
        length = length + i
    while length > bufsize:
        bufsize = bufsize * 2
        buf = <int *>realloc(buf, bufsize*sizeof(int))
        mbuf = <_kenlm.WordIndex *>realloc(mbuf, bufsize*sizeof(_kenlm.WordIndex))

    i = 0
    # same as rule.Phrase.subst
    for j from 0 <= j < ewords.n:
        if sym.isvar(ewords.syms[j]):
            for word in antstates[sym.getindex(ewords.syms[j])-1]:
                buf[i] = word
                i = i + 1
        else:
            buf[i] = ewords.syms[j]
            i = i + 1

    # kenlm wants this in reverse order
    for i from 0 <= i < length:
        if not sym.isvar(buf[i]) and buf[i] != HOLE:
            mbuf[length-i-1] = map_word(ngram, buf[i], mapdigits, lowercase)

    sum = 0.0

    edge = 0
    remnant = []

    for i from 0 <= i < length:
        if buf[i] == HOLE:
            edge = i+1
        elif i-edge+1 >= order:
            p = -ngram.model.BaseFullScoreForgotState(&mbuf[length-i], &mbuf[length-i+order-1], mbuf[length-i-1], &dummy).prob
            if p > ceiling:
                p = ceiling
            sum = sum + p
        elif edge == 0:
            remnant.append(buf[i])

    if edge != 0 or length >= order:
        remnant.append(HOLE)
        if edge < length - (order-1):
            edge = length - (order-1)
        for i from edge <= i < length:
            remnant.append(buf[i])
    return (tuple(remnant), sum)

cdef float lookup_words2(Model ngram, state, char mapdigits, bint lowercase, float ceiling):
    """This is the same as lookup_words1 but:
       - takes a single state instead of a rule plus child states
       - doesn't compute a result state
       - consequently, even looks up symbols at left edge
       - skips over the START symbol at very left edge
    """
    cdef int length, edge, i, flag, n
    cdef float sum, p
    global mbuf, buf, bufsize
    cdef int order = ngram.model.Order()
    cdef _kenlm.State dummy

    length = len(state)
    while length > bufsize:
        bufsize = bufsize * 2
        buf = <int *>realloc(buf, bufsize*sizeof(int))
        mbuf = <_kenlm.WordIndex *>realloc(mbuf, bufsize*sizeof(_kenlm.WordIndex))

    sum = 0.0

    edge = 0      # how far back do we find ngrams
    flag = 1      # 1 means compute incomplete ngrams at edge

    for i from 0 <= i < length:
        if state[i] == HOLE:
            edge = i+1
            flag = 0
        elif sym.isvar(state[i]):
            edge = i+1
            flag = 1
        else:
            # kenlm wants this in reverse order
            mbuf[length-i-1] = map_word(ngram, state[i], mapdigits, lowercase)
            if (i-edge+1 >= order or  # we have a complete n-gram
                (flag and not               # or want an incomplete n-gram
                 (i == 0 and state[i] == START))): # except START at very left edge
                n = i-edge+1
                if n > order:
                    n = order
                p = -ngram.model.BaseFullScoreForgotState(&mbuf[length-i], &mbuf[length-i+n-1], mbuf[length-i-1], &dummy).prob
                if p > ceiling:
                    p = ceiling
                sum = sum + p

    return sum

# temporary area used by map_word
cdef int wordbufsize
cdef char *wordbuf
wordbufsize = 100
wordbuf = <char *>malloc(wordbufsize)

cdef int map_word(Model ngram, int s, char c, bint lc):
    cdef char *word
    cdef int i, n, result
    cdef int oldcachesize
    global wordbuf, wordbufsize

    if s == START:
        return ngram.vocab.BeginSentence()
    elif s == STOP:
        return ngram.vocab.EndSentence()

    word = <char *>sym.tostring(s)
    n = len(word)
    if n+1 > wordbufsize:
        wordbuf = <char *>realloc(wordbuf, (n+1)*sizeof(char))
        wordbufsize = n+1

    # copy word and map digits
    for i from 0 <= i < n:
        if c and c'0' <= word[i] <= c'9':
            wordbuf[i] = c
        else:
            wordbuf[i] = word[i]

        if lc:
            wordbuf[i] = tolower(wordbuf[i])
    wordbuf[n] = c'\0'

    return ngram.vocab.Index(wordbuf)

LOGZERO = -999.0

class LanguageModel(model.Model):
    def __init__(self, f, feat, mapdigits=0, lowercase=False, ceiling=-LOGZERO):
        cdef char *tmpstr
        model.Model.__init__(self)

        self.feat = feat

        self.positive = True
        self.factorable = True

        if type(mapdigits) is str:
            tmpstr = mapdigits
            self.mapdigits = tmpstr[0]
        else:
            self.mapdigits = mapdigits
        self.lowercase = lowercase
        self.ceiling = ceiling

        if log.level >= 1:
            log.write("Reading language model (kenlm) from %s...\n" % f)

        self.ngram = Model(f)

        # to do: p_unk

        self.order = self.ngram.order

    ### Decoder interface
        
    def transition(self, r, antstates, i, j, j1=None):
        (state, dcost) = lookup_words1(self.ngram, r.e, antstates, self.mapdigits, self.lowercase, self.ceiling)
        return (state, svector.Vector(self.feat, dcost))

    def bonus(self, lhs, state):
        if state is not None:
            result = svector.Vector(self.feat, lookup_words2(self.ngram, state, self.mapdigits, self.lowercase, self.ceiling))
        else:
            result = svector.Vector()
        return result

    def estimate(self, r):
        result = svector.Vector(self.feat, lookup_words2(self.ngram, r.e, self.mapdigits, self.lowercase, self.ceiling))
        return result

    def finaltransition(self, state):
        result = svector.Vector(self.feat, lookup_words2(self.ngram,
                                                       (START,)+state+(STOP,), self.mapdigits, self.lowercase, self.ceiling))
        return result

    def strstate(self, state):
        result = []
        prev = None
        for e in state:
            if e == HOLE:
                result.append("...")
            else:
                if prev and prev != HOLE:
                    result.append(" ")
                result.append(sym.tostring(e))
            prev = e
        return "".join(result)
