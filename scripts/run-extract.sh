#!/bin/bash
#$ -q *@@nlp
#$ -pe mpi-* 16

module load java/1.7
export JAVA_HOME=/afs/crc.nd.edu/x86_64_linux/java/src/jdk1.7.0_25/

NLP=/afs/crc.nd.edu/group/nlp
export HADOOP_PREFIX=$NLP/software/hadoop/1.2.1

echo "Creating Hadoop cluster"

CLUSTER=cluster.$JOB_ID
$HADOOP_PREFIX/sbin/sge_hadoop.py $CLUSTER
export HADOOP_CONF_DIR=$CLUSTER/conf

echo "Extracting grammar"

./extract.sh

for dst in $(find $CLUSTER/logs -type l); do 
    src=$(readlink $dst)
    rm -rf $dst
    /bin/cp -r $src $dst
done
